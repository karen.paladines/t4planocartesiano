
                 # Autora: "Karen Paladines"
                 # Email: "karen.paladines@unl.edu.ec"

#Crear un programa en Python que utilice la librería pygame

import pygame
import random
blanco=255,255, 255
negro=0,0,0
rojo=255,0,0

pygame.init()
Dimensiones = (600, 600)
Pantalla = pygame.display.set_mode(Dimensiones)
pygame.display.set_caption("Mostrar Plano Cartesiano")

#TEXTO DEL PLANO CARTESIANO
Fuente = pygame.font.Font(None, 25)
Texto = Fuente.render("Representar Plano Cartesiano", True, rojo)
X= Fuente.render("X", True, negro)
Y = Fuente.render("Y", True, negro)
Cuadrado =Fuente.render("640 X 480", True, rojo)
Uno = Fuente.render("-1", True, negro)
Dos = Fuente.render("-2", True, negro)
Uno1 = Fuente.render("-1", True, negro)
Dos2 = Fuente.render("-2", True, negro)
Var = Fuente.render("640", True, rojo)
Var2 = Fuente.render("480", True, rojo)


Terminar = False
reloj = pygame.time.Clock()

while not Terminar:
     for Evento in pygame.event.get():
        if Evento.type == pygame.QUIT:
            Terminar = True

     Pantalla.fill(blanco)
     pygame.draw.line(Pantalla, rojo, [595,300],[5,300],3)
     pygame.draw.line(Pantalla, rojo, [300, 5], [300, 595], 3)
     pygame.draw.rect(Pantalla, blanco, (300, 300, 250, 120), 0)
     Pantalla.blit(Texto, [10, 10])
     Pantalla.blit(X, [585, 280])
     Pantalla.blit(Y, [310, 585])
     Pantalla.blit(Uno, [290, 310])
     Pantalla.blit(Dos, [290, 340])
     Pantalla.blit(Uno1, [310, 280])
     Pantalla.blit(Dos2, [330, 280])
     Pantalla.blit(Var, [540, 280])
     Pantalla.blit(Var2, [275, 410])
     Pantalla.blit(Cuadrado, [400, 350])

     pygame.display.flip()
     reloj.tick(25)
pygame.quit()